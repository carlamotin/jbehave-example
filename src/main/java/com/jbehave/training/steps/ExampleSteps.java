package com.jbehave.training.steps;

import org.jbehave.core.annotations.*;
import org.jbehave.core.steps.Steps;

import com.jbehave.training.model.Calc;

import junit.framework.Assert;

public class ExampleSteps extends Steps {
	
	Calc calc;
	
	@Given("two numbers $num1 and $num2")
	public void getTwoNumbers(@Named("num1") String num1, @Named("num2") String num2) {
		int n1 = Integer.parseInt(num1);
		int n2 = Integer.parseInt(num2);
		calc = new Calc(n1, n2);
	}	
	
	@When("I $operation those numbers")
	public void doOperationWithNumbers(@Named("operation") String operation) {
		calc.setResult(calc.getNum1() + calc.getNum2());	
	}
	
	@Then("I should see the result")
	public void showResult() {
		System.out.println("Sum result: " + calc.getResult());
	}
	

}
